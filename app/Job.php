<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = "jobs";

    protected $fillable = ['title', 'description', 'requirements', 'offer'];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function applications(){
        return $this->hasMany('App\Application');
    }
}
