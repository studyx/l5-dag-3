<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = "companies";

    protected $fillable = ['name', 'contacts', 'website_url', 'picture_url'];

    public function address()
    {
        return $this->belongsTo('App\Address');
    }

    public function jobs(){
        return $this->hasMany('App\Job');
    }
}
