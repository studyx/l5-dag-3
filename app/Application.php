<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $table = "applications";

    protected $fillable = ['name', 'contacts', 'email', 'qualifications', 'previous_work_experence', 'cv'];

    public function job()
    {
        return $this->belongsTo('App\Job');
    }
}
