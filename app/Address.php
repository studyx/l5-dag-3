<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = "addresses";

    protected $fillable = ['name', 'longitude', 'latitude'];

    public function companies(){
        return $this->hasMany('App\Company');
    }
}
