<?php

namespace App\Http\Controllers;

use App\Address;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $input = $request->all();

        $company = new Company($input);

        $imageName = $request->file('picture_url')->getClientOriginalName();


        $request->file('picture_url')->move(
            base_path() . '/public/uploads/companies/', $imageName
        );

        $company->picture_url = $imageName;

        if ($input['address']) {

            $response = \GoogleMaps::load('geocoding')
                ->setParam (['address' =>$input['address']['name']])
                ->get('results.geometry.location');

            $input['address']['latitude'] = $response["results"][0]['geometry']['location']['lat'];
            $input['address']['longitude'] = $response["results"][0]['geometry']['location']['lng'];

            $address = new Address($input['address']);
            $address->save();
            $company->address()->associate($address);
        }

        $company->save();

        Session::flash('flash_message', 'Company succesvol aangemaakt.');

        return redirect('admin/companies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);

        if(is_null($company))
        {
            abort('404');
        }

        return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);

        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
