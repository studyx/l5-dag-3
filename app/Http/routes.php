<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

// ADMIN ROUTES

Route::group(['prefix' => 'admin'], function () {
    Route::resource('companies', 'CompaniesController');
    Route::resource('jobs', 'JobsController');
    Route::resource('applications', 'ApplicationController');
});

