{!! Form::open(['url' => 'admin/applications', 'novalidate' => 'novalidate', 'files' => true]) !!}

<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
</div>
<div class="form-group">
    {!! Form::label('contacts', 'Contacts:') !!}
    {!! Form::textarea('contacts', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('qualifications', 'Qualifications:') !!}
    {!! Form::textarea('qualifications', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('previous_work_experence', 'prev owkr exp:') !!}
    {!! Form::textarea('previous_work_experence', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cv', 'cv:') !!}
    {!! Form::file('cv', null, ['class' => 'form-control']) !!}
</div>

{!! Form::hidden('job_id', $jobid) !!}

<div class="form-group">
    {!! Form::submit("verzend", ['class' => 'btn btn-primary form-control']) !!}
</div>

{!! Form::close() !!}