<div class="form-group">
    {!! Form::label('name', 'Naam:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
</div>
<div class="form-group">
    {!! Form::label('contacts', 'Contactinfo:') !!}
    {!! Form::textarea('contacts', null, ['class' => 'form-control textarea']) !!}
</div>
<div class="form-group">
    {!! Form::label('website_url', 'Website:') !!}
    {!! Form::text('website_url', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('picture_url', 'Afbeelding:') !!}
    {!! Form::file('picture_url', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('address[name]', 'Adres:') !!}
    {!! Form::textarea('address[name]', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::submit($submitText, ['class' => 'btn btn-primary form-control']) !!}
</div>