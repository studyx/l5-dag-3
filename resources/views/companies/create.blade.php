@extends('layouts.admin')

@section('content')

    <h1 class="page-header">Bedrijf aanmaken</h1>

    {!! Form::open(['url' => 'admin/companies', 'files' => true]) !!}
        @include('companies.form', ['submitText' => 'Bedrijf aanmaken'])
    {!! Form::close() !!}


@stop

