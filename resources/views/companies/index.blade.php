@extends('layouts.admin')

@section('content')

    @forelse($companies as $company)
        <h2>{{ $company->name }}</h2>
        <p>{{ $company->contacts }}</p>
        <img src="/uploads/companies/{{ $company->picture_url }}" alt="{{ $company->name }}" />
    @empty
        <p>Er zijn nog geen bedrijven toegevoegd.</p>
    @endforelse
@stop