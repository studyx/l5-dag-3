@extends('layouts.admin')

@section('content')

    <h1>{{ $job->title }}</h1>


    @include('applications.form', ['jobid' => $job->id])

@stop