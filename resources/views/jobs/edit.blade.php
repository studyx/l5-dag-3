@extends('layouts.admin')

@section('content')


    {!! Form::model($company, ['method' => 'PUT', 'url' => 'admin/companies/' . $company->id]) !!}
        @include('companies.form', ['submitText' => 'Bedrijf wijzigen'])
    {!! Form::close() !!}

@stop