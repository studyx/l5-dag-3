<div class="form-group">
    {!! Form::label('title', 'Titel:') !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'required' => true]) !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Beschrijving:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('requirements', 'Requirements:') !!}
    {!! Form::textarea('requirements', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('offer', 'Offer:') !!}
    {!! Form::textarea('offer', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('company_id', 'Bedrijf:') !!}
    {!! Form::select('company_id', $companies, null, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::submit($submitText, ['class' => 'btn btn-primary form-control']) !!}
</div>