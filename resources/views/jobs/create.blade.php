@extends('layouts.admin')

@section('content')

    <h1 class="page-header">Job aanmaken</h1>

    {!! Form::open(['url' => 'admin/jobs']) !!}
        @include('jobs.form', ['submitText' => 'Job aanmaken'])
    {!! Form::close() !!}
@stop