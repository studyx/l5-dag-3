var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        'vendor/bootstrap/css/bootstrap.min.css',
        'vendor/font-awesome/css/font-awesome.min.css',
        'css/sb-admin-2.min.css'
    ], 'public/css/app.css', 'resources/assets');

    mix.scripts([
        'vendor/jquery/jquery.min.js',
        'vendor/bootstrap/js/bootstrap.min.js'
    ], 'public/js/app.js', 'resources/assets');

    mix.copy('resources/assets/vendor/font-awesome/fonts', 'public/fonts');
});
